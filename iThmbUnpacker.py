######################################################################################
#iThmbUnpacker by obaby 
#Unpack the .ithmb file 
#Date:2014.5.15 
#Site:http://www.h4ck.org.cn
######################################################################################

import sys
import os,os.path,platform,struct


def ithmbunpack120(filename):
        i120width=120
        i120height=120
        i120size=i120height*i120width*2

        i120header_data=b'BM'
        i120header_data+=struct.pack('l', i120size+0x36)
        i120header_data+=b'\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00\x00\x00'
        i120header_data+=struct.pack('l', i120width)+struct.pack('l', -i120height)
        i120header_data+=b'\x01\x00\x10\x00\x00\x00\x00\x00'
        i120header_data+=struct.pack('l', i120size)
        i120header_data+=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        infile =open(filename,'rb')
        bitmapfileno =1
        while True:
                data = infile.read(i120size)
                outfile = open(os.path.dirname(os.path.realpath(__file__))+'/bmp120/'+str(bitmapfileno) +'.bmp','wb+')
                outfile.write(i120header_data)
                outfile.write(data)
                outfile.close
                infile.read(28)
                bitmapfileno = bitmapfileno+1
                if not data:break

def ithmbunpack158(filename):
        width=160
        height=158
        size=height*width*2

        header_data=b'BM'
        header_data+=struct.pack('l', size+0x36)
        header_data+=b'\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00\x00\x00'
        header_data+=struct.pack('l', width)+struct.pack('l', -height)
        header_data+=b'\x01\x00\x10\x00\x00\x00\x00\x00'
        header_data+=struct.pack('l', size)
        header_data+=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        infile =open(filename,'rb')
        bitmapfileno =1
        while True:
                data = infile.read(size)
                outfile = open(os.path.dirname(os.path.realpath(__file__))+'/bmp158/'+str(bitmapfileno) +'.bmp','wb+')
                outfile.write(header_data)
                outfile.write(data)
                outfile.close
                infile.read(28)
                bitmapfileno = bitmapfileno+1
                if not data:break

def PrintRuler():
        for i in range(0,16):    
            print "%3s" % hex(i) ,    
        print    
        for i in range(0,16):    
            print "%-3s" % "#" ,    
        print

def iOS7ithmb2Pix(ithmb):
        return{
                "3303":22,
                "3309":64,
                "3319":157,
                "4031":120
                }.get(ithmb,110)
######################################################################################
#ithmb type in iOS7:
#               iOS7type Width Height RealWidth RealHeight
#                 3303 [24  22 ]
#                 3309 [64  64]
#                 3319 [160 157]
#                 3141 [160 158 150 150 true true 2],
#                 3041 [80 79 75 75 true true 1],
#                 
#                 4131 [240 240 240 240 false],
#                 4031 [120 120 120 120 false],
#                 4132 [64 64 64 64 false],
#                 4032 [32 32 32 32 false],
#                 
#                 4140 [336 332 304 332 false false 4],
#                 4040 [168 166 152 166 false false 2],
######################################################################################
def ithmbunpack(filename):
       
        ifilename = os.path.basename(filename)
        if (ifilename.find("x") != -1):
                itmbsize = int(ifilename.split(".")[0].split("x")[0])
        else:
                itmbsize = iOS7ithmb2Pix(ifilename.split(".")[0])
                
        print ('The bitmap is in resoulation :'+str(itmbsize))
        
        PrintRuler()
        
        if itmbsize ==22: #done
                width =24
                height =22
        if itmbsize ==64:
                width =64
                height =64
        if itmbsize ==157: #done 
                width =160
                height =157
                
        if itmbsize == 32:
                width =32
                height = 32
        if itmbsize == 79:
                width = 80
                height = 79
        if itmbsize == 120:
                width = 120
                height = 120
        if itmbsize == 158:
                width =160
                height = 158
        if itmbsize == 166:
                width = 168
                height = 166

        
        size=height*width*2

        header_data=b'BM'
        header_data+=struct.pack('l', size+0x36)
        header_data+=b'\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00\x00\x00'
        header_data+=struct.pack('l', width)+struct.pack('l', -height)
        header_data+=b'\x01\x00\x10\x00\x00\x00\x00\x00'
        header_data+=struct.pack('l', size)
        header_data+=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        infile =open(filename,'rb')
        bitmapfileno =1
        while True:
                data = infile.read(size)
                outfile = open(os.path.dirname(os.path.realpath(__file__))+'/bmp/'+str(bitmapfileno) +'.bmp','wb+')
                outfile.write(header_data)
                outfile.write(data)
                outfile.close
                #infile.read(28) #print the data between the bitmap file
                index= 0
                for f in range (0,28):
                        temp=infile.read(1)    
                        if len(temp) == 0:    
                                break    
                        else:    
                                print "%3s" % temp.encode('hex'),    
                                index=index+1    
                        if index == 16:    
                                index=0    
                                print
                print("\n")
        
                bitmapfileno = bitmapfileno+1
                if not data:break
        print("\nTotal: " + str(bitmapfileno) +" unpacked!")


def printusage():
        print("================================================================")
        print(sys.argv[0].split("\\")[-1] + " By obaby")
        print("Usage:\n    " +sys.argv[0].split("\\")[-1] + " <.ithmb>")
        print(".ithmb is the file u want to unpacked!")
        print("================================================================")
	pass

if __name__=="__main__":

        if len(sys.argv)>1:
                filename = sys.argv[1]
                print("================================================================")
                print(sys.argv[0].split("\\")[-1] + " By obaby \n")
                print("Start to unpack the file :\n    " + filename)
                ithmbunpack(filename)
                print("================================================================")                
	else:
                printusage()
                

